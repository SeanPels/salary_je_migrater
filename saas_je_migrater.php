<?php
date_default_timezone_set('UTC');
error_reporting(E_ALL);
define('APP_DIR','forefront');
define('APP_NAME','forefront');

require_once('intacctws-php/api_session.php');
require_once('intacctws-php/api_post.php');
require_once('util/iautil.php');
require_once('util/message_logger.php');

//$arr = str_getcsv('data.csv');
$options = getopt("",array("object::","cnyid::","fields:","query::","sessionid::","outfile::","docparid::","max_rows::"));
$cnyid = $options['cnyid'] ?? die("--cnyid is required");

loadIni("app.$cnyid.ini");

function map_journal($incoming) {
    $map = array (
        'SAASSTAT' => 'SaaSSTAT',
        'SAASGL' => 'SaaSJRNL',
    );

    return  $map[$incoming] ?? $map['SAASGL'];
}


global $acct_map;

// old accountno => new accountno
$acct_map = array (
    '99099' => 'DB099',
    '99015' => 'DB001',
    '99016' => 'DB004',
    '99035' => 'DB041',
    '99017' => 'DB003',
    '99020' => 'DB005',
    '99045' => 'DB011',
    '99030' => 'DB098',
    '99025' => 'DB006',
    '99040' => 'DB012',
    '99050' => 'DB039',
    '99010' => 'DB002',
    '99055' => 'DB021',
    '99060' => 'DB022',
    '99061' => 'DB023',
    '91501' => 'DB095',
    '91502' => 'DB096',
    '99051' => 'DB035',
    '99056' => 'DB029',
    '99105' => 'DBQ01',
    '99106' => 'DBQ04',
    '99120' => 'DBQ41',
    '99110' => 'DBQ05',
    '99107' => 'DBQ03',
    '99125' => 'DBQ11',
    '99115' => 'DBQ98',
    '99151' => 'DBC31',
    '99150' => 'DBC39',
    '99130' => 'DBQ39',
    '99140' => 'DBC01',
    '99141' => 'DBC04',
    '99135' => 'DBC02',
    '99142' => 'DBC03',
    '99145' => 'DBC41',
    '99100' => 'DBQ02',
    '99131' => 'DBQ35',
);
function map_account($incoming) {
    global $acct_map;

    return  $acct_map[$incoming] ?? die("$incoming not found in map.");
}


/*
                $readMore       = [
                    'function' => [
                        '@controlid' => 'read_more',
                        'readMore'   => [
                            'object' => $object,
                        ]
                    ]
                ];
                $response = api_post::sendFunctions($readMore, $this->session);
 */

try {
    $iasession = getIaSession();

    $query = array (
        'object' => "GLENTRY",
        'filter' => array (
            'in' => array (
                'field' => array ('ACCOUNTNO'),
                'value' => array_keys($acct_map)
            ),
        ),
        'select' => array (
            'field' => array (
                'RECORDNO','ACCOUNTNO','TR_TYPE','LOCATION','CUSTOMERID','ITEMID','TRX_AMOUNT','CURRENCY','EXCHANGE_RATE','GLBATCH.RECORDNO', 'GLBATCH.BATCH_TITLE','GLBATCH.JOURNAL','GLBATCH.BATCH_DATE','GLBATCH.SAAS_ORDER_URL','GLBATCH.SAAS_SOURCE_DOCUMENT','GLBATCH.MEGAENTITYID',
                'DEPARTMENT','PROJECTID','VENDORID','EMPLOYEEID','CLASSID','GLDIMPRODUCT','GLDIMREVENUE_TYPE'
            )
        ),
        'orderby' => array (
            'order' => array (
                'field' => 'ENTRY_DATE',
                //'field' => 'GLBATCH.RECORDNO',
                'ascending' => '',
            )
        ),
        'pagesize' => 2000,
        'options' => array (
            'showprivate' => 'true'
        )
    );

    $lines = api_post::query($iasession,$query,100);

    //dbg($lines);

    file_put_contents($cnyid . "_source_data.obj",serialize($lines));
    //die("\nX_X\n");

    //dbg(api_post::getLastRequest());

    //$lines = api_post::readByQuery("RECURDOCUMENTENTRY","", "",$iasession,'100000000',api_returnFormat::CSV);
   /*
   $query = array (
        'object' => "GLENTRY",
        'filter' => array (
            'in' => array (
                'field' => array ('ACCOUNTNO'),
                'value' => array_keys($acct_map)
            ),
        ),
        'select' => array (
            'field' => array (
                'RECORDNO','ACCOUNTNO','TR_TYPE','LOCATION','CUSTOMERID','ITEMID','TRX_AMOUNT','CURRENCY','EXCHANGE_RATE','GLBATCH.RECORDNO', 'GLBATCH.BATCH_TITLE','GLBATCH.JOURNAL','GLBATCH.BATCH_DATE','GLBATCH.SAAS_ORDER_URL','GLBATCH.SAAS_SOURCE_DOCUMENT','GLBATCH.MEGAENTITYID'
            )
        ),
        'orderby' => array (
            'order' => array (
                'field' => 'ENTRY_DATE',
                'ascending' => '',
            )
        ),
        'pagesize' => 2000,
        'options' => array (
            'showprivate' => 'true'
        )
    );
   */

    //$lines = api_post::query($iasession,$query);

    //dbg(api_post::getLastResponse());
    if (!is_array($lines)) {
        $lines = array();
    }

    $lines_by_key = array();
    $new_jes = array(

    );

    foreach ($lines as $l) {
        $entity = $l['GLBATCH.MEGAENTITYID'];
        if (!isset($new_jes[$l['GLBATCH.RECORDNO']])) {
            $journal = map_journal($l['GLBATCH.JOURNAL']);

            $new_jes[$entity] = $new_jes[$entity] ?? array();

            if (!isset($new_jes[$entity][$l['GLBATCH.RECORDNO']])) {
                $new_jes[$entity][$l['GLBATCH.RECORDNO']] = array(
                    'BATCH_TITLE' => $l['GLBATCH.BATCH_TITLE'],
                    'BATCH_DATE' => $l['GLBATCH.BATCH_DATE'],
                    'JOURNAL' => $journal,
                    'SAAS_ORDER_URL' => $l['GLBATCH.SAAS_ORDER_URL'],
                    'SAAS_SOURCE_DOCUMENT' => $l['GLBATCH.SAAS_SOURCE_DOCUMENT'],
                    'ENTRIES' => array(
                        'GLENTRY' => array()
                    )
                );
            }
        }
        $l['ACCOUNTNO'] = map_account($l['ACCOUNTNO']);
        //$lines_by_key[$l['GLBATCH.RECORDNO']] = $lines_by_key[$l['GLBATCH.RECORDNO']] ?? array();
        //$lines_by_key[$l['GLBATCH.RECORDNO']][] = $l;
        $new_jes[$entity][$l['GLBATCH.RECORDNO']]['ENTRIES']['GLENTRY'][] = $l;
    }
    print_r($new_jes);
    file_put_contents($cnyid . "_je_migration_data.obj",serialize($new_jes));

} catch (Exception $ex) {
    manage_api_exception($ex);
}
?>